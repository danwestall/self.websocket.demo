<?php
/**
 * Plugin Name: WP Realtime
 * Plugin URI: https://github.com/gedex/wp-wp-realtime/
 * Description: This plugin enables realtime optoon for wordpress provided by websockets
 * Version: 0.1.0
 * Author: Dan Westall
 * Author URI: http://www.dan-westall.co.uk
 * Text Domain: wp-realtime
 * Domain Path: /languages
 * License: GPL v2 or later
 * Requires at least: 3.6
 * Tested up to: 3.9
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

require_once __DIR__ . '/includes/autoloader.php';

// Register the autoloader.
WP_Realtime_Autoloader::register( 'WP_Realtime', trailingslashit( plugin_dir_path( __FILE__ ) ) . '/includes/' );

// Runs this plugin.
$GLOBALS['realtime'] = new WP_Realtime_Plugin();
$GLOBALS['realtime']->run( __FILE__ );
