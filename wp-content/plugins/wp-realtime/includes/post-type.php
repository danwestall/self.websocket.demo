<?php

/**
 * Custom post type where each post stores HipChat integration settings.
 */
class WP_Realtime_Post_Type {

	/**
	 * Post type name.
	 *
	 * @var string
	 */
	public $name = 'poll';

	/**
	 * @var WP_Realtime_Plugin
	 */
	private $plugin;

	public function __construct( WP_Realtime_Plugin $plugin ) {
		$this->plugin = $plugin;

		// Register custom post type to store HipChat integration records.
		add_action( 'init', array( $this, 'register_post_type' ) );

		// Enqueue scripts/styles and disables autosave for this post type.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		add_action( 'rest_api_init', array( $this, 'register_poll_api_meta' ), 10, 3 );

		add_action( 'wp_enqueue_scripts', array( $this, 'cpt_defaults' ), 10 );

	}

	public function register_post_type() {

		$args = array(
			'description'           => '',
			'public'                => true,
			'publicly_queryable'    => true,
			'show_in_nav_menus'     => true,
			'show_in_admin_bar'     => true,
			'exclude_from_search'   => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_icon'             => 'dashicons-list-view',
			'can_export'            => true,
			'delete_with_user'      => true,
			'hierarchical'          => false,
			'has_archive'           => false,
			'query_var'             => false,
			'map_meta_cap'          => false,
			'capabilities'          => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'manage_options',
				'read_post'              => 'manage_options',
				'delete_post'            => 'manage_options',
				// primitive/meta caps
				'create_posts'           => 'manage_options',
				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'manage_options',
				'edit_others_posts'      => 'manage_options',
				'publish_posts'          => 'manage_options',
				'read_private_posts'     => 'manage_options',
				// primitive caps used inside of map_meta_cap()
				'read'                   => 'manage_options',
				'delete_posts'           => 'manage_options',
				'delete_private_posts'   => 'manage_options',
				'delete_published_posts' => 'manage_options',
				'delete_others_posts'    => 'manage_options',
				'edit_private_posts'     => 'manage_options',
				'edit_published_posts'   => 'manage_options',
			),
			'rewrite'               => false,
			// What features the post type supports.
			'supports'              => array(
				'title',
				'editor'
			),
			'labels'                => array(
				'name'               => __( 'Poll', 'wp-realtime' ),
				'singular_name'      => __( 'Poll', 'wp-realtime' ),
				'menu_name'          => __( 'Poll', 'wp-realtime' ),
				'name_admin_bar'     => __( 'Poll', 'wp-realtime' ),
				'add_new'            => __( 'Add New', 'wp-realtime' ),
				'add_new_item'       => __( 'Add New Poll', 'wp-realtime' ),
				'edit_item'          => __( 'Edit Poll', 'wp-realtime' ),
				'new_item'           => __( 'New Poll', 'wp-realtime' ),
				'view_item'          => __( 'View Poll', 'wp-realtime' ),
				'search_items'       => __( 'Search Poll', 'wp-realtime' ),
				'not_found'          => __( 'No Poll found', 'wp-realtime' ),
				'not_found_in_trash' => __( 'No Poll in trash', 'wp-realtime' ),
				'all_items'          => __( 'Polls', 'wp-realtime' ),
			),
			'show_in_rest'          => true,
			'rest_controller_class' => 'WP_REST_Posts_Controller',

		);

		// Register the post type.
		register_post_type( $this->name, $args );
	}

	public function enqueue_scripts() {

		if ( $this->name === get_post_type() ) {

			wp_enqueue_script(
			// Handle.
				'hipchat-admin-js',

				// Src.
				$this->plugin->plugin_url . 'js/admin.js',

				// Deps
				array( 'jquery' ),

				// Ver.
				filemtime( $this->plugin->plugin_path . 'js/admin.js' )
			);
		}
	}

	function register_poll_api_meta() {

		if ( ! function_exists( 'register_api_field' ) ) {

			return false;

		}

		register_api_field( $this->name,
			'poll_options',
			array(
				'get_callback'    => array( $this, 'get_polling_options'),
				'update_callback' => null,
				'schema'          => null,
			)
		);

		register_api_field( $this->name,
			'display_results',
			array(
				'get_callback'    => array( $this, 'get_poll_meta_data' ),
				'update_callback' => null,
				'schema'          => null,
			)
		);

		register_api_field( $this->name,
			'poll_results',
			array(
				'get_callback'    => array( $this, 'get_poll_results' ),
				'update_callback' => null,
				'schema'          => null,
			)
		);
	}

	function get_poll_meta_data( $object, $field_name, $request ) {

		return get_post_meta( $object['id'], $field_name, true );

	}

	function get_polling_options( $object, $field_name, $request ) {

		$poll_options = get_post_meta( $object['id'], '_wp_polling_options', true );

		return $poll_options;

	}

	function get_poll_results( $object, $field_name, $request ) {

		$vote_summary = array();

		$poll_options = get_post_meta( $object['id'], '_wp_polling_options', true );

		foreach ( $poll_options as $key => $option ) {

			$option_meta_key = sprintf( '_vote_%s', $key );

			$results = get_post_meta( $object['id'], $option_meta_key, false );

			$vote_summary[ $key ] = count( $results );

		}

		return $vote_summary;

	}

}
