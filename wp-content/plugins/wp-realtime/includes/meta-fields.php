<?php


class WP_Realtime_Meta_Fields {

	/**
	 * @var WP_Realtime_Plugin
	 */
	private $plugin;

	public function __construct( WP_Realtime_Plugin $plugin ) {

		$this->plugin = $plugin;

		add_action( 'cmb2_admin_init', array( $this, 'register_poll_meta' ) );

		add_action( 'update_post_metadata', array( $this, 'delete_option_votes' ), 10, 5 );

	}

	function register_poll_meta() {

		$prefix = '_wp_polling_';

		$cmb_group = new_cmb2_box( array(
			'id'           => $prefix . 'metabox',
			'title'        => __( 'Poll Options', 'wp-realtime' ),
			'object_types' => array( $this->plugin->post_type->name ),
		) );

		$cmb_group->add_field( array(
			'name' => __( 'Display Results', 'cmb2' ),
			'desc' => __( 'Display Poll results on clients', 'cmb2' ),
			'id'   => $prefix . 'display_results',
			'type' => 'checkbox',
		) );

		$group_field_id = $cmb_group->add_field( array(
			'id'          => $prefix . 'options',
			'type'        => 'group',
			'description' => __( 'Add options to the poll', 'wp-realtime' ),
			'options'     => array(
				'group_title'   => __( 'Option {#}', 'wp-realtime' ), // {#} gets replaced by row number
				'add_button'    => __( 'Add Another Option', 'wp-realtime' ),
				'remove_button' => __( 'Remove option', 'wp-realtime' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb_group->add_group_field( $group_field_id, array(
			'name' => __( 'Label', 'wp-realtime' ),
			'id'   => 'title',
			'type' => 'text',
		) );

	}


	/**
	 *
	 * When a poll option is changed we need to delete all the vote for this poll option
	 *
	 * @param $change
	 * @param $object_id
	 * @param $meta_key
	 * @param $meta_value
	 * @param $prev_value
	 *
	 * @return null
	 */
	function delete_option_votes( $change, $object_id, $meta_key, $meta_value, $prev_value ) {

		if( ! in_array( get_post_meta( $object_id ) , $this->plugin->manager->get_realtime_post_types() ) ) {

			return null;

		}

		//get the repeater field contains the poll options
		$poll_options = get_post_meta( $object_id, '_wp_polling_options', true );

		foreach ( $poll_options as $option ) {

			$option_meta_key = sprintf( '_vote_%s', $option['label'] );

			if( $meta_key !== $option_meta_key ) {

				continue;

			}

			//if we get here then the meta_key that is changing is a poll options so we need to delete the all the votes
			delete_post_meta( $object_id, $option_meta_key );

		}

		/*
		 * When the poll options have been changed and the old votes removed then we need to trigger update on the realtime side
		 */
		do_action( 'poll_option_change', $object_id );

		return null;

	}


}