<?php

class WP_Realtime_Poll_Vote {

	/**
	 * @var WP_Realtime_Plugin
	 */
	private $plugin;

	public function __construct( WP_Realtime_Plugin $plugin ) {

		$this->plugin = $plugin;

		add_action( 'wp_ajax_vote', array( $this, 'register_vote' ) );

		add_action( 'wp_ajax_nopriv_vote', array( $this, 'register_vote') );

	}

	public function register_vote() {


		check_ajax_referer('security-' . date('dmy'), 'security');

		$connection_id = absint( $_POST['connectionID'] );
		$option_id     = absint( $_POST['voteOption'] );
		$post_id       = absint( $_POST['pollID'] );


		if( $this->has_voted( $post_id, $connection_id ) ) {

			return false;

		}

		$vote_key = sprintf( '_vote_%s', $option_id );

		add_post_meta( $post_id, $vote_key, $connection_id );

		do_action( 'poll_vote_change', $post_id );


	}

	public function has_voted( $post_id, $connection_id ) {

		global $wpdb;

		$query = $wpdb->prepare(
			"SELECT * FROM $wpdb->postmeta WHERE post_id = %s AND meta_value = %s",
			$post_id,
			$connection_id
		);

		$vote = $wpdb->get_results( $query );

		if( is_wp_error( $vote ) ) {

			return false;

		}

		if ( ! empty( $vote ) ) {

			return true;

		}

		return false;

	}

	public function can_vote( $post_id, $connection_id){

		return true;

	}

	public function voting_open( $post_id ) {

		return true;

	}

}

