<?php

class WP_Realtime_Manager {

	/**
	 * @var WP_Realtime_Plugin
	 */
	private $plugin;

	public function __construct( WP_Realtime_Plugin $plugin ) {

		$this->plugin = $plugin;

		add_action( 'save_post', array( $this, 'update_realtime_poll_state' ), 100, 1 );

		add_action( 'poll_vote_change', array( $this, 'update_realtime_poll_state' ), 10, 1 );

		add_action( 'poll_option_change', array( $this, 'update_realtime_poll_state' ) );
	}

	public function update_realtime_poll_state( $post_id ) {

		//tag action as topic for websocket subscriptions
		$action = current_filter();

		$response = $this->get_poll_state( $post_id );

		//topic for realtime subscriptions
		$response['subscription'] = $this->get_action_map()[ $action ];

		$this->broadcast( $post_id, $response );

	}

	public function broadcast( $post_id, $response ) {

		//check is realtime is possible on current enviroment
		if ( ! class_exists( 'ZMQContext' ) ) {

			return false;

		}

		if ( ! in_array( get_post_type( $post_id ), $this->get_realtime_post_types() ) ) {

			return false;

		}

		//check for is live
		if ( ! $this->is_realtime_live() ) {

			return false;

		}


		//send to realtime
		$context = new ZMQContext();
		$socket  = $context->getSocket( ZMQ::SOCKET_PUSH, 'my pusher' );
		$socket->connect( "tcp://localhost:5555" );

		$socket->send( json_encode( $response ) );

	}

	function get_realtime_post_types() {


		return (array) apply_filters( 'realtime_post_types', array( $this->plugin->post_type->name ) );

	}

	function is_realtime_live() {

		return (bool) apply_filters( 'realtime_live', true );

	}


	function get_poll_state( $post_id ) {

		//this should be a diff not the complete object again.
		$state = $this->rest_internal_request( 'GET', sprintf( '/wp/v2/poll/%s', $post_id ) );

		return $state;

	}


	private function check_diff_multi( $array1, $array2 ) {
		$result = array();
		foreach ( $array1 as $key => $val ) {
			if ( array_key_exists( $key, $array2 ) ) {
				if ( is_array( $val ) && is_array( $array2[ $key ] ) && ! empty( $val ) ) {
					$result[ $key ] = $this->check_diff_multi( $val, $array2[ $key ] );
				}
			} else {
				$result[ $key ] = $val;
			}
		}

		return $result;
	}

	/**
	 * Make an internal REST request
	 *
	 * @global WP_REST_Server $wp_rest_server ResponseHandler instance (usually WP_REST_Server).
	 *
	 * @param $request_or_method WP_REST_Request|string A WP_REST_Request object or a request method
	 * @param $path (optional) if the path is not specific in the rest request, specify it here
	 * @param $data (optional) option data for the request.
	 *
	 * @return WP_Error|mixed
	 */
	function rest_internal_request( $request_or_method, $path = null, $data = array() ) {

		/** @var WP_REST_Server $wp_rest_server */
		global $wp_rest_server;

		if ( empty( $wp_rest_server ) ) {
			/**
			 * Filter the REST Server Class.
			 *
			 * This filter allows you to adjust the server class used by the API, using a
			 * different class to handle requests.
			 *
			 * @since 4.4.0
			 *
			 * @param string $class_name The name of the server class. Default 'WP_REST_Server'.
			 */
			$wp_rest_server_class = apply_filters( 'wp_rest_server_class', 'WP_REST_Server' );
			$wp_rest_server       = new $wp_rest_server_class;

			/**
			 * Fires when preparing to serve an API request.
			 *
			 * Endpoint objects should be created and register their hooks on this action rather
			 * than another action to ensure they're only loaded when needed.
			 *
			 * @since 4.4.0
			 *
			 * @param WP_REST_Server $wp_rest_server Server object.
			 */
			do_action( 'rest_api_init', $wp_rest_server );
		}

		if ( is_a( $request_or_method, 'WP_REST_Request' ) ) {
			$request = $request_or_method;
		} else {
			$request = new WP_REST_Request( $request_or_method, $path );

			foreach ( $data as $k => $v ) {
				$request->set_param( $k, $v );
			}
		}

		$result = $wp_rest_server->dispatch( $request );

		if ( $result->is_error() ) {
			return $result->as_error();
		}

		$result = $wp_rest_server->response_to_data( $result, ! empty( $data['_embed'] ) );

		return $result;
	}

	function get_action_map() {

		$action_topics = array(
			'save_post'          => 'poll_change',
			'poll_vote_change'   => 'poll_change',
			'poll_option_change' => 'poll_change'
		);

		return apply_filters( 'polling_action_top_map', $action_topics );

	}

}