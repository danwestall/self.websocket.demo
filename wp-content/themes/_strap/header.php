<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="self.websocket.demo">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/angular.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/autobahn.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script>
		angular.module('self.websocket.demo', [ ])
			.filter('to_trusted', ['$sce', function($sce){
				return function(text) {
					return $sce.trustAsHtml(text);
				};
			}])
			.controller('SomeController', function ($scope) {

				$scope.data = {};
				$scope.test = 't';
				$scope.hasVoted = false;
				$scope.connectionID = 0;

				try {
					var conn = new ab.Session('ws://' + jQuery(location).attr('hostname') + ':8080', function () {

							$scope.connectionID = conn['_session_id'];
							conn.subscribe('poll_change', function (topic, data) {
								$scope.data = data;

								console.log( data );
								$scope.$apply();

							});


						},
						function () {
							console.warn('WebSocket Live connection closed');
						},
						{'skipSubprotocolCheck': true});
				} catch (e) {
					console.log(e);
					self.socket = false;
//					self.fallbackstart();
				}

				$scope.optionVote = function ( optionIndex ) {
					console.log(optionIndex);
					jQuery.ajax({
						url: js.ajaxurl,
						type: 'POST',
						cache: false,
						data: {
							security: js.security,
							action: 'vote',
							connectionID: $scope.connectionID,
							voteOption: optionIndex,
							pollID: js.post_id
						},
						dataType: 'JSON',
						success: function (results) {
							$scope.hasVoted = true;

						}
					});

				};

				var GetDefault = function() {

					jQuery.ajax({
						url: 'http://' +jQuery(location).attr('hostname') + '/wp-json/wp/v2/poll/' + js.post_id,
						type: 'GET',
						cache: false,
						dataType: 'JSON',
						success: function (results) {
							$scope.data = results;
							$scope.$apply();

							console.log( $scope.data );
						}
					});
				};



				GetDefault();


			});
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site container">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<p class="site-description"><?php bloginfo( 'description' ); ?></p>
		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">