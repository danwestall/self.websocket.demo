<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

<div id="" class="content-area">
	<main id="main" class="col-md-12" role="main">

		<section ng-controller="SomeController">

				<table class="table">
					<tr ng-repeat="option in data.poll_options">
						<td>{{ option.title }}</td>
						<td>{{ data.poll_results[ $index ] }}</td>
					</tr>

				</table>


			<h3>{{ data.title.rendered }}</h3>

			<div ng-bind-html="data.content.rendered | to_trusted"></div>

			<div style="padding: 30px 0px;">

				<button ng-repeat="option in data.poll_options" type="button" class="btn btn-primary btn-lg btn-block" ng-click="optionVote($index)">{{ option.title }}</button>

			</div>

			<pre>

				{{ data | json }}

			</pre>



		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
