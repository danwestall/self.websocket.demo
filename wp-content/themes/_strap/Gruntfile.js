'use strict';
module.exports = function (grunt) {

    var jsfiles = {
        build: {
            files: {
                //'public/js/modernizr.min.js' : 'tmp/modernizr.js',
                'public/js/app.min.js' : 'tmp/app.js',
                'public/js/dragndrop.min.js' : 'assets/js/draganddrop.js',
                'public/js/flash.min.js' : 'assets/js/angular-flash.js',
                'public/js/vex.min.js' : 'bower_components/vex/js/vex.combined.min.js',
                'public/js/jsPlumb.min.js' : 'bower_components/jsplumb/dist/js/jquery.jsPlumb-1.6.4-min.js',
                'public/js/tournament.results.min.js' : 'tmp/tournament.results.js'
            }
        },
        theme: {
            files: [{
                expand: true,
                cwd: 'assets/js/theme_defaults/',
                src: '*.js',
                dest: 'public/js/',
                ext: '.min.js'
            }]
        },
        singleapps: {
            files: [{
                expand: true,
                cwd: 'tmp/singleapps/',
                src: '*.js',
                dest: 'public/js/',
                ext: '.min.js'
            }]
        },
        brackets: {
            files: [{
                expand: true,
                cwd: 'assets/js/brackets/',
                src: '*.js',
                dest: 'public/js/brackets/',
                ext: '.min.js'
            }]
        }         
    };


    grunt.registerTask("prepareModules", "Finds and prepares modules for concatenation.", function() {

        // get all module directories
        grunt.file.expand("assets/js/singleapps/*").forEach(function (dir) {

            // get the module name from the directory name
            var dirName = dir.substr(dir.lastIndexOf('/')+1);

            // get the current concat object from initConfig
            var concat = grunt.config.get('concat') || {};

            // create a subtask for each module, find all src files
            // and combine into a single js file per module
            concat[dirName] = {
                src: [dir + '/**/*.js'],
                dest: 'tmp/singleapps/' + dirName + '.min.js'
            };

            // add module subtasks to the concat task in initConfig
            grunt.config.set('concat', concat);
        });
    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // package options
        less: {
            mini: {
                options: {
                    cleancss: true, // minify
                    report: 'min' // minification results
                },
                expand: true, // set to true to enable options following options:
                cwd: "assets/less/", // all sources relative to this path
                src: "*.less", // source folder patterns to match, relative to cwd
                dest: "public/css/", // destination folder path prefix
                ext: ".css", // replace any existing extension with this value in dest folder
                flatten: true  // flatten folder structure to single level
            }
        },

        express: {
            server: {
                options: {
                    port: 3000,
                    hostname: 'localhost',
                    bases: 'public'
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'assets/js/*.js'
            ]
        },
        concat: {
            basic: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/lodash/dist/lodash.min.js',
                    'assets/js/jquery.counteverest.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-animate/angular-animate.min.js',
                    'bower_components/angular-sanitize/angular-sanitize.min.js',
                    'bower_components/angular-messages/angular-messages.min.js',
                    'bower_components/angucomplete-alt/dist/angucomplete-alt.min.js',
                    'assets/js/ui-bootstrap-custom-0.12.1.min.js',                    
                    'bower_components/jQuery.mmenu/src/js/jquery.mmenu.min.all.js',
                    'bower_components/simpleStorage/simpleStorage.js',
                    'bower_components/fancybox/source/jquery.fancybox.js',
                    'bower_components/jquery-cycle2/build/jquery.cycle2.min.js',
                    'assets/js/social.share.js',
                    'assets/js/app.js'
                ],
                dest: 'tmp/app.js'
            },
            extras: {
                src: [
                    'assets/js/tournament.results.js'
                ],
                dest: 'tmp/tournament.results.js'
            }
        },
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'assets/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'public/img/'
                }]
            }
        },
        ngAnnotate:{
            singleapps: {
                files: [ {
                    expand: true,
                    cwd: 'tmp/singleapps/',
                    src: '*.js',
                    dest: 'tmp/singleapps/'
                }]
            }
        },
        uglify: {
            options: {
              mangle: false,
              sourceMap: false
            },
            build: jsfiles.build,
            theme: jsfiles.theme,
            singleapps: jsfiles.singleapps,
            brackets: jsfiles.brackets
        },
        clean: {
            dist: [
                'tmp/**',
                'public/img/**',
                'public/js/**'
            ]
        },
        copy: {
          fonts: {
            files : [
              {
                expand:true,
                cwd:'assets',
                src: ['fonts/*'],
                dest: 'public/fonts/',
                filter:'isFile',
                flatten: true
              }
            ]
          },
          favicon: {
            files : [
              {
                expand:true,
                cwd:'assets',
                src: ['favicon/*'],
                dest: 'public/favicon/',
                filter:'isFile',
                flatten: true
              }
            ]
          },
          mmenucss: {
            files : [
              {
                expand:true,
                cwd:'bower_components',
                src: ['jQuery.mmenu/src/css/jquery.mmenu.all.css'],
                dest: 'public/css/',
                filter:'isFile',
                flatten: true
              }
            ]
          },
          img: {
            files : [
              {
                expand:true,
                cwd:'assets',
                src: ['img/*','!img/Thumbs.db'],
                dest: 'public/img/',
                filter:'isFile',
                flatten: true
              }
            ]
          },
          devjsbuild: jsfiles.build,
          devjstheme: jsfiles.theme,
          devjssingleapps: jsfiles.singleapps,
          devjsbrackets: jsfiles.brackets,
          templates: {
            files : [
              {
                expand:true,
                cwd:'assets',
                src: ['html_templates/*','!html_templates/Thumbs.db'],
                dest: 'public/html_templates/',
                filter:'isFile',
                flatten: true
              }
            ]
          }
        },
        compress: {
          main: {
            options: {
              archive: 'theme.zip'
            },
            files:[
              {expand: true, src: ['*.php'], dest: ''},
              {expand: true, src: ['functions/**'], dest: ''},
              {expand: true, src: ['public/**'], dest: ''},
              {expand: true, src: ['style.css'], dest: ''},
              {expand: true, src: ['screenshot.png'], dest: ''},
            ]
          }
        },
        focus: {
            prd: {
                exclude: ['jsdev','js2dev','js3dev','jsbracketsdev']
            },
            dev: {
                exclude: ['js','js2','js3','jsbrackets']
            }
        },
        watch: {
            options: {
                livereload: true,
            },
            compass: {
                files: ['assets/sass/**/*.{scss,sass}'],
                tasks: ['compass']
            },
            less: {
                files: ['assets/less/*.less', 'assets/less_vars/*.less'],  //watched files
                tasks: ['less'],                          //tasks to run
                options: {                    //reloads the browser
                    atBegin: true
                }
            },
            css: {
                files: ['public/css/*'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: [
                    'assets/js/*.js'
                ],
                tasks: ['concat', 'uglify:build'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            js2: {
                files: [
                    'assets/js/theme_defaults/*.js'
                ],
                tasks: ['uglify:theme'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            js3: {
                files: [
                    'assets/js/singleapps/**/*.js'
                ],
                tasks: ['prepareModules','concat','ngAnnotate:singleapps','uglify:singleapps'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            jsbrackets: {
                files: [
                    'assets/js/brackets/*.js'
                ],
                tasks: ['uglify:brackets'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },            
            jsdev: {
                files: [
                    'assets/js/*.js'
                ],
                tasks: ['concat', 'copy:devjsbuild'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            js2dev: {
                files: [
                    'assets/js/theme_defaults/*.js'
                ],
                tasks: ['copy:devjstheme'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            js3dev: {
                files: [
                    'assets/js/singleapps/**/*.js'
                ],
                tasks: ['prepareModules','concat','copy:devjssingleapps'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            jsbracketsdev: {
                files: [
                    'assets/js/brackets/*.js'
                ],
                tasks: ['copy:devjsbrackets'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },             
            img: {
                files: [
                    'assets/img/**'
                ],
                tasks: ['copy:img'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            },
            templates: { //do a minfiy html for prd
                files: [
                    'assets/html_templates/*.html'
                ],
                tasks: ['copy:templates'],
                options: {
                    atBegin: true
                }
            },
            livereload: {
                // Here we watch the files the sass task will compile to
                // These files are sent to the live reload server after sass compiles to them
                options: { livereload: true },
                files: ['public/css/**/*']
            }
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-focus');
    grunt.loadNpmTasks('grunt-ng-annotate');

    // Register default tasks
    grunt.registerTask('default', [
        'clean',
        'copy:fonts',
        'copy:favicon',
        'copy:mmenucss',
        'express:server',
        'focus:prd'
    ]);

    // DEV Task (no uglify)
    grunt.registerTask('dev',[
        'clean',
        'copy:fonts',
        'copy:favicon',
        'copy:mmenucss',
        'express:server',
        'focus:dev'
    ]);

    //aka zip to theme zip
    grunt.registerTask('deploy',['compress:main']);

};
